from django.shortcuts import render

# Create your views here.
from rest_framework.viewsets import ModelViewSet          # add this
from rest_framework.response import Response
from .serializers import PersonalcustomerSerializer     # add this
from .models import Personalcustomer                     # add this

class PersonalcustomerView(ModelViewSet):       # add this
    serializer_class = PersonalcustomerSerializer          # add this
    queryset = Personalcustomer.objects.all()