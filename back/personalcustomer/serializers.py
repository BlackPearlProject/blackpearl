from rest_framework import serializers
from .models import Personalcustomer



class PersonalcustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Personalcustomer
        fields = ('id', 'name', 'mobile', 'source','location')