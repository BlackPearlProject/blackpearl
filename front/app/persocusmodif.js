import React, { Component } from 'react';
import {  Text, View } from 'native-base';
import { Container, Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Content } from 'native-base';
import { StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';


export default class Persocusmodif extends React.Component {
    constructor(props) {
        super(props);
        this.state = { pcustomer: this.props.navigation.state.params.customer,
          text1:'',text2:'',text3:'',text4:'',
                       
      
      };
      }
      componentWillMount(){
        this.setState({text1:this.state.pcustomer.name,text2:this.state.pcustomer.mobile,text3:this.state.pcustomer.source,text4:this.state.pcustomer.location});
      }
      submit(){
        let data ={};
        data.name=this.state.text1;
        data.mobile=this.state.text2;
        data.source=this.state.text3;
        data.location=this.state.text4;
        
      
        var url='http://a11918d3.ngrok.io/api/Personalcustomers/'
        fetch(url,{
          method:'POST',
          body:JSON.stringify(data),
          headers: new Headers({
            'Content-Type':'application/json'
          })
        }).then(res=> res.json())
        .catch(error=>console.error('Error:',error))
        .then(response=>console.log('Success',response));
        this.props.navigation.goBack()
        }
        delete(){
            /* this.state.savearray.splice(key,1);
             this.setState({savearray:this.state.savearray})*/
             
             var url='http://a11918d3.ngrok.io/api/Personalcustomers/';
             var item = this.state.pcustomer.id;
             
           fetch(url + item + '/',{
             method: 'DELETE'
           })
           this.props.navigation.goBack()
        }

        edit(){
          let data ={};
        data.name=this.state.text1;
        data.mobile=this.state.text2;
        data.source=this.state.text3;
        data.location=this.state.text4;
          var url='http://a11918d3.ngrok.io/api/Personalcustomers/';
          var item = this.state.pcustomer.id;
          
          fetch(url + item + '/',{
            method: 'PUT',
            body:JSON.stringify(data),
            headers: new Headers({
              'Content-Type':'application/json'
            })
            
          }).then(res=> res.json())
          .catch(error=>console.error('Error:',error))
          .then(response=>console.log('Success',response));
          this.props.navigation.goBack()

        }


    render() {
        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent onPress={()=>{ this.props.navigation.goBack();}} >
                    <Icon name='arrow-back' />
                  </Button>
                </Left>
                <Body>
                  <Title>edit customer</Title>
                </Body>
                <Right />
              </Header>
              <View  >
              <Text style={styles.text}> Name:</Text>
              <TextInput placeholder='name'
             style={{fontSize:20}}
             defaultValue={this.state.pcustomer.name}
             onChangeText={(text1)=>this.setState({text1})}
             
             
            
             ></TextInput>
             <Text style={styles.text}> Mobile:</Text>

             <TextInput placeholder='mobile'
             style={{fontSize:20}}
             onChangeText={(text2)=>this.setState({text2})}
             defaultValue={this.state.pcustomer.mobile}
             
             
             ></TextInput>
             <Text style={styles.text}> source:</Text>

             <TextInput placeholder='source'
             style={{fontSize:20}}
             onChangeText={(text3)=>this.setState({text3})}
             defaultValue={this.state.pcustomer.source}
             
             ></TextInput>
             <Text style={styles.text}> Location:</Text>

             <TextInput placeholder='location'
             style={{fontSize:20}}
             onChangeText={(text4)=>this.setState({text4})}
             defaultValue={this.state.pcustomer.location}
            
             ></TextInput>


       
         
        

             </View>
              <Content/>
              












              <Footer style={{backgroundColor:'#FFAD38'}}>
                <FooterTab>
               
            <Button style={styles.head} onPress={() =>this.edit()}>
            <Icon style={{color:'#fff'}} name='construct' />
            <Text style={{color:'#fff'}}>edit</Text>
            
            </Button>
            <Button style={styles.head} onPress={() =>this.delete()}>
            <Icon style={{color:'#fff'}} name='trash' />
            <Text style={{color:'#fff'}}>delete</Text>
            
            </Button>
           
                  
                </FooterTab>
              </Footer>
            </Container>
          );
    }
  }
  const styles = StyleSheet.create({
  
    head:{
      backgroundColor: '#FFAD38',
    },
    text:{
        fontSize:40,
        color:'#544B3E',
        
      }
   
  });