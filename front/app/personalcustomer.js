import React, { Component } from 'react';
import { StyleSheet,ActivityIndicator ,ScrollView,RefreshControl} from 'react-native';
import { Container,Text,  Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Content, View, Fab } from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Persocusadd from './persocusadd'
import Persocusmodif from './persocusmodif'
import Save from './save'




class Personalcustomer extends Component {

    constructor(props) {
        super(props);
        this.state = {refreshing: false,isloding:true
          ,data:null};
      }
      componentDidMount(){
        return fetch('http://a11918d3.ngrok.io/api/Personalcustomers/')
        .then((response)=>response.json() ) 
        .then( (responsejson )=>{
           this.setState({
             isloding:false,
             data:responsejson,
           })
        })
        .catch((error)=>{console.log(error)});
      
      }
      _onRefresh = () => {
        this.setState({refreshing: true});
        fetch('http://a11918d3.ngrok.io/api/Personalcustomers/')
        .then((response)=>response.json() ) 
        .then( (responsejson )=>{
           this.setState({
             isloding:false,
             data:responsejson,
             refreshing: false,
           })
        })
        .catch((error)=>{console.log(error)});
      
      }
      modifmet(val){
        this.props.navigation.navigate('Persocusmodif',{customer:val})
      }
      delsave(val){
       /* this.state.savearray.splice(key,1);
        this.setState({savearray:this.state.savearray})*/
        
        var url='http://a11918d3.ngrok.io/api/Personalcustomers/'
        var item = val.id;
        
      fetch(url + item + '/',{
        method: 'DELETE'
      })
      this._onRefresh()}
      
      navigate(){
        let data ={};
        data.name='';
        data.mobile='';
        data.source='';
        data.location='';
        this.props.navigation.navigate('Persocusadd',{customer:data})
      }
  
    render() { if(this.state.isloding){
        return (
          <View> 
            <ActivityIndicator/>
          </View>
        )
      }else{
          let customers= this.state.data.map((val,key)=>{
        return <Save key={key} keyval={key} val={val}
        delmet={()=>this.delsave(val)}
        modifmet={()=>this.modifmet(val)}
      />
        
        
        
        
        
        /*<View key={key} >
           <Text>{val.id}  </Text>
           <Text>{val.title}  </Text>
           <Text>{val.description}  </Text>
        </View>*/
      })

        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent  onPress={()=>{this.props.navigation.openDrawer();}}>
                    <Icon name='menu' style={styles.TextStyle}/>
                  </Button>
                </Left>
                <Body>
                  <Title style={styles.TextStyle}>Personal customer</Title>
                </Body>
                
              </Header>
              <ScrollView 
             refreshControl={
             <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            />
        }
             >
             <Content>
             
             {customers}
             
             
             
             
             
             
             
             
             </Content>
              </ScrollView> 
              <View>
              <Fab
            
            
            
            style={{ backgroundColor: '#FFAD38' }}
            position="bottomRight"
            onPress={() => this.navigate()} style={styles.head}>
            <Icon name="add" />
            
          </Fab>
              </View>
                  
              








              
              <Footer  >
                <FooterTab style={{backgroundColor:'#FFAD38'}}>
                <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Home")} >
          <Icon style={{color:'#fff'}} name='home' />
          <Text style={{color:'#fff'}}>home</Text>
          </Button>
          
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Saves")} >
          <Icon style={{color:'#fff'}} name='archive' />
          <Text style={{color:'#fff'}}>Saves</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Calendar")}>
          <Icon style={{color:'#fff'}} name='calendar' />
          <Text style={{color:'#fff'}}>calendar</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Settings")}>
          <Icon style={{color:'#fff'}} name='settings' />
          <Text style={{color:'#fff'}}>settings</Text>
          </Button>
            
          </FooterTab>
        </Footer>
      </Container>
   );}
}
}

const styles = StyleSheet.create({
  
  head:{
    backgroundColor: '#FFAD38',
  },
  
  
 
});




  const AppNavigator = createStackNavigator({
    Home1:{screen :Personalcustomer,
        navigationOptions: {
            header: null}
    },
    Persocusadd:{screen :Persocusadd,
        navigationOptions: {
           title:"add customer",
           header: null }},
    Persocusmodif:{screen :Persocusmodif,
            navigationOptions: {
               title:"edit customer",
               header: null }},       
  });
  
  export default createAppContainer(AppNavigator);

 
