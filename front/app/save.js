
import React, { Component } from 'react';
import { View,Text } from 'react-native';
import { Button, Accordion, SwipeRow, Icon, Left, Right, Body, Container } from 'native-base';




export default class Save extends React.Component {
    
    render() {
      return (
        <View key={this.props.keyval} >
         
          <SwipeRow
            leftOpenValue={75}
            rightOpenValue={-75}
            left={
              <Button success onPress={this.props.modifmet}>
                <Icon active name="construct" />
              </Button>
            }
            body={
              <View style={{flex:1,justifyContent: "center",alignItems: "center"}}>
                
                <Text style={{fontSize:20,color:'#202020'}}>{this.props.val.name}</Text>
                <Text style={{fontSize:20,color:'#202020'}}>{this.props.val.mobile}</Text>
                
        
                
               
              </View>
            }
            right={
              <Button danger onPress={this.props.delmet}>
                <Icon active name="trash" />
              </Button>
            }
          />
        </View>
      );
    }
  }