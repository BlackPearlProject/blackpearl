
import React, { Component } from 'react';
import { Text, Card, CardItem, Thumbnail, View } from 'native-base';
import { StyleSheet ,ActivityIndicator} from 'react-native';
import { Container, Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon,  Content } from 'native-base';
import { ScrollView ,RefreshControl} from 'react-native';
import Save from './save'



export default class Screen2 extends Component {
  static navigationOptions =()=>({
    drawerIcon:()=> <Icon name='calendar' />
  });

  constructor(props) {
    super(props);
    this.state = {refreshing: false,isloding:true
      ,data:null};
  }
  componentDidMount(){
    return fetch('http://376041a6.ngrok.io/api/todos/')
    .then((response)=>response.json() ) 
    .then( (responsejson )=>{
       this.setState({
         isloding:false,
         data:responsejson,
       })
    })
    .catch((error)=>{console.log(error)});
  
  }
  _onRefresh = () => {
    this.setState({refreshing: true});
    fetch('http://376041a6.ngrok.io/api/todos/')
    .then((response)=>response.json() ) 
    .then( (responsejson )=>{
       this.setState({
         isloding:false,
         data:responsejson,
         refreshing: false,
       })
    })
    .catch((error)=>{console.log(error)});
  
  }
  delsave(val){
   /* this.state.savearray.splice(key,1);
    this.setState({savearray:this.state.savearray})*/
    
    var url='http://376041a6.ngrok.io/api/todos/'
    var item = val.id;
    
  fetch(url + item + '/',{
    method: 'DELETE'
  })
  this._onRefresh()
  
  
  }
    render() {
      if(this.state.isloding){
        return (
          <View> 
            <ActivityIndicator/>
          </View>
        )
      }else{
        let movies= this.state.data.map((val,key)=>{
          return <Save key={key} keyval={key} val={val}
          delmet={()=>this.delsave(val)}
        />
          
          
          
          
          
          /*<View key={key} >
             <Text>{val.id}  </Text>
             <Text>{val.title}  </Text>
             <Text>{val.description}  </Text>
          </View>*/
        })


        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent  onPress={()=>{this.props.navigation.openDrawer();}}>
                    <Icon name='menu' />
                  </Button>
                </Left>
                <Body>
                  <Title>Calendar</Title>
                  
                </Body>
                <Right />
              </Header>
              
             <ScrollView 
             refreshControl={
             <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            />
        }
             >
             <Content>
             
             {movies}
             
             
             
             
             
             
             
             
             </Content>
              </ScrollView> 
              
              <Footer>
             
          <FooterTab style={{backgroundColor:'#FFAD38'}}>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Home")} >
          <Icon style={{color:'#fff'}} name='home' />
          <Text style={{color:'#fff'}}>home</Text>
          </Button>
          
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Saves")} >
          <Icon style={{color:'#fff'}} name='archive' />
          <Text style={{color:'#fff'}}>Saves</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Calendar")}>
          <Icon style={{color:'#fff'}} name='calendar' />
          <Text style={{color:'#fff'}}>calendar</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Settings")}>
          <Icon style={{color:'#fff'}} name='settings' />
          <Text style={{color:'#fff'}}>settings</Text>
          </Button>
          
        </FooterTab>
      </Footer>
    </Container>
  );}
}
}
const styles = StyleSheet.create({

head:{
  backgroundColor: '#FFAD38',
},

});