import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Container,Text,  Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Content, View } from 'native-base';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Screen31 from './screen31'



class Screen3 extends Component {
  
    render() {
        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent  onPress={()=>{this.props.navigation.openDrawer();}}>
                    <Icon name='menu' style={styles.TextStyle}/>
                  </Button>
                </Left>
                <Body>
                  <Title style={styles.TextStyle}>Settings</Title>
                </Body>
                <Right />
              </Header>
              <View  style={{alignItems:"center", marginTop:200}}>
             <Text style={{fontSize:60}}>
               Settings
             </Text>
             <View  style={{alignItems:"center"}}>
                  <Button  onPress={() => this.props.navigation.navigate('screen31')} style={styles.head}>
                  <Text style={styles.TextStyle}>Advanced Settings</Text>
                  </Button>
              </View>
             </View>
              <Content/>
                  
              
              
              <Footer  >
                <FooterTab style={{backgroundColor:'#FFAD38'}}>
                <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Home")} >
          <Icon style={{color:'#fff'}} name='home' />
          <Text style={{color:'#fff'}}>home</Text>
          </Button>
          
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Saves")} >
          <Icon style={{color:'#fff'}} name='archive' />
          <Text style={{color:'#fff'}}>Saves</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Calendar")}>
          <Icon style={{color:'#fff'}} name='calendar' />
          <Text style={{color:'#fff'}}>calendar</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Settings")}>
          <Icon style={{color:'#fff'}} name='settings' />
          <Text style={{color:'#fff'}}>settings</Text>
          </Button>
            
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  
  head:{
    backgroundColor: '#FFAD38',
  },
  
  
 
});




  const AppNavigator = createStackNavigator({
    Home1:{screen :Screen3,
        navigationOptions: {
            header: null}
    },
    screen31:{screen :Screen31,
        navigationOptions: {
           title:"screen4",
           header: null }},
  });
  
  export default createAppContainer(AppNavigator);

 
