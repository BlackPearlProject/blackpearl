
import React, { Component } from 'react';
import {View ,StyleSheet,ScrollView } from 'react-native';
import { Container,Text, Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Content, Fab } from 'native-base';

import Save from './save'
import { TextInput } from 'react-native-gesture-handler';

export default class Screen1 extends Component {
  static navigationOptions =({navigation})=>({
    drawerIcon:()=> <Icon name='archive' />
  });
  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date(),active: 'true',
    savearray : [],
    savetext:'',
    savetext1:'',
   };
    this.setDate = this.setDate.bind(this);
  }
  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }
    render() {

      let saves = this.state.savearray.map((val,key)=>{
        return <Save key={key} keyval={key} val={val}
          delmet={()=>this.delsave(key)}
        />

      });
        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent  onPress={()=>{this.props.navigation.openDrawer();}}>
                    <Icon name='menu' />
                  </Button>
                </Left>
                <Body>
                  <Title>Saves</Title>
                </Body>
                <Right />

              </Header>
             <View >
             <TextInput placeholder='right save name'
             style={{fontSize:20}}
             onChangeText={(savetext)=>this.setState({savetext})}
             value={this.state.savetext}
             >

             </TextInput>


             <TextInput placeholder='right note'
             style={{fontSize:20}}
             onChangeText={(savetext1)=>this.setState({savetext1})}
             value={this.state.savetext1}
             ></TextInput>


            
           
             
             


             </View>
             
              <Content>
              {saves}
              
              
              </Content>
              <View>
              <Fab
            
            
            
            style={{ backgroundColor: '#FFAD38' }}
            position="bottomRight"
            onPress={this.addsave.bind(this)}>
            <Icon name="add" />
            
          </Fab>
              </View>
              



             
              
              <Footer>
             
          <FooterTab style={{backgroundColor:'#FFAD38'}}>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Home")} >
          <Icon style={{color:'#fff'}} name='home' />
          <Text style={{color:'#fff'}}>home</Text>
          </Button>
          
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Saves")} >
          <Icon style={{color:'#fff'}} name='archive' />
          <Text style={{color:'#fff'}}>Saves</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Calendar")}>
          <Icon style={{color:'#fff'}} name='calendar' />
          <Text style={{color:'#fff'}}>calendar</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Settings")}>
          <Icon style={{color:'#fff'}} name='settings' />
          <Text style={{color:'#fff'}}>settings</Text>
          </Button>
          
        </FooterTab>
      </Footer>
    </Container>
  );
}
addsave(){
  if (this.state.savetext){
    
    this.state.savearray.push({
      'nom': this.state.savetext,
      'note': this.state.savetext1,

    });
    this.setState({savearray: this.state.savearray})
    this.setState({savetext:'',savetext1:''})
  }

}
delsave(key){
  this.state.savearray.splice(key,1);
  this.setState({savearray:this.state.savearray})
}
}
const styles = StyleSheet.create({

head:{
  backgroundColor: '#FFAD38',
},

});