import React, { Component } from 'react';
import {  Text, View } from 'native-base';
import { Container, Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Content } from 'native-base';
import { StyleSheet } from 'react-native';


export default class Screen31 extends React.Component {
    render() {
        return (
      
            <Container >
              
              <Header style={styles.head}>
                <Left>
                  <Button transparent onPress={()=>{ this.props.navigation.goBack();}} >
                    <Icon name='arrow-back' />
                  </Button>
                </Left>
                <Body>
                  <Title>Adv.Settings</Title>
                </Body>
                <Right />
              </Header>
              <View  style={{alignItems:"center", marginTop:200}}>
             <Text style={{fontSize:60}}>
               Adv.Settings
             </Text>


             </View>
              <Content/>
              
              <Footer style={{backgroundColor:'#FFAD38'}}>
                <FooterTab>
                <Button style={styles.head}>
            <Icon style={{color:'#fff'}} name='add' />
            <Text style={{color:'#fff'}}>add</Text>
            
            </Button>
            <Button style={styles.head}>
            <Icon style={{color:'#fff'}} name='construct' />
            <Text style={{color:'#fff'}}>edit</Text>
            
            </Button>
            <Button style={styles.head}>
            <Icon style={{color:'#fff'}} name='trash' />
            <Text style={{color:'#fff'}}>delete</Text>
            
            </Button>
           
                  
                </FooterTab>
              </Footer>
            </Container>
          );
    }
  }
  const styles = StyleSheet.create({
  
    head:{
      backgroundColor: '#FFAD38',
    },
   
  });