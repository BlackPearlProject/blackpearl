import React, { Component } from 'react';
import { Container, Header, Title, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Content, Form, Item, Label, Input, Picker, View } from 'native-base';

import { StatusBar,StyleSheet } from 'react-native'
import { TextInput } from 'react-native-gesture-handler';





export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: true, selected2: undefined ,text1:'',text2:'',};
  }
  static navigationOptions =({navigation})=>({
    drawerIcon:()=> <Icon name='home' />
  });
  async componentWillMount() {
    await Expo.Font.loadAsync({
      Roboto: require("native-base/Fonts/Roboto.ttf"),
      Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
      Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf"),
    });
    this.setState({ loading: false });
  }
 
  componentDidMount() {
    StatusBar.setHidden(true);
  }
  onValueChange2(value: string) {
    this.setState({
      selected2: value
    });
  }


  submit(){
  let data ={};
  data.title=this.state.text1;
  data.description=this.state.text2;
  data.completed='false';
  

  var url='http://376041a6.ngrok.io/api/todos/'
  fetch(url,{
    method:'POST',
    body:JSON.stringify(data),
    headers: new Headers({
      'Content-Type':'application/json'
    })
  }).then(res=> res.json())
  .catch(error=>console.error('Error:',error))
  .then(response=>console.log('Success',response));
  
  }
  render() {
    if (this.state.loading) {
      return <Expo.AppLoading />;
    }
    return (
      
      <Container >
        
        <Header style={styles.head}>
          <Left>
            <Button transparent  onPress={()=>{this.props.navigation.openDrawer();}}>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Home</Title>
          </Body>
          <Right />
        </Header>
        <View >
        <TextInput placeholder='todoname'
             style={{fontSize:20}}
             onChangeText={(text1)=>this.setState({text1})}
             value={this.state.savetext}
             >

             </TextInput>


             <TextInput placeholder='description'
             style={{fontSize:20}}
             onChangeText={(text2)=>this.setState({text2})}
             value={this.state.savetext1}
             ></TextInput>


         <Button style={styles.head} onPress={() =>this.submit()}>
         
          <Text style={{color:'#fff'}}>add</Text>
          </Button>





           
        </View>
        <Content/>
        <Footer>

        
          <FooterTab style={{backgroundColor:'#FFAD38'}}>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Home")} >
          <Icon style={{color:'#fff'}} name='home' />
          <Text style={{color:'#fff'}}>home</Text>
          </Button>
          
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Saves")} >
          <Icon style={{color:'#fff'}} name='archive' />
          <Text style={{color:'#fff'}}>Saves</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Calendar")}>
          <Icon style={{color:'#fff'}} name='calendar' />
          <Text style={{color:'#fff'}}>calendar</Text>
          </Button>
          <Button style={styles.head} onPress={() =>this.props.navigation.navigate("Settings")}>
          <Icon style={{color:'#fff'}} name='settings' />
          <Text style={{color:'#fff'}}>settings</Text>
          </Button>
            
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  
  head:{
    backgroundColor: '#FFAD38',
  },
 
});

