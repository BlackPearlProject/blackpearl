import { createDrawerNavigator, createAppContainer, DrawerItems } from 'react-navigation';
import { Image } from 'react-native';
import HomeScreen from './app/Home'
import Screen1 from './app/screen1'
import Screen2 from './app/screen2'
import Screen3 from './app/screen3'
import Personalcustomer from './app/personalcustomer'
import { Container, Header, Text, Footer, FooterTab, Button, Left, Right, Body, Icon, Content} from 'native-base';
import React, { Component } from 'react';



const Cd = (props) => (
  <Container>
    <Header style={{height:250,backgroundColor:'#fff',}}>
    
     <Left>
      <Image style={{width:130,height:130,borderRadius:50,}} source={require('./assets/avatar.jpg')}/>
     </Left>
     <Right>
       <Text style={{marginRight:12,fontSize:20}}>Black Pearl</Text>
     </Right>
    

    </Header>
    <Content>
      <DrawerItems {...props}/>
    </Content>
  </Container>

);

const MyDrawerNavigator = createDrawerNavigator({
   
  
    Home: HomeScreen,
    Saves: Screen1,
    Calendar: Screen2,
    Settings:{screen:Screen3,
      navigationOptions :{
      drawerIcon:()=> <Icon name='settings' />
      
    }},
  Personalcustomer: Personalcustomer,
  
  
},{
  initialRouteName:'Home',
  contentComponent : Cd,
  drawerOpenRoute : 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute:'DrawerToggle',

});

const MyApp = createAppContainer(MyDrawerNavigator);
export default MyApp;











/*
export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
*/